----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.05.2019 10:01:49
-- Design Name: 
-- Module Name: BASCULE_D - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------



library IEEE;
use IEEE.std_logic_1164.all;
entity basc is   
port(D : in std_logic;      
clk : in std_logic;      
Q : out std_logic); 
end basc; 

architecture comporte of basc is
begin 
process(clk) begin   
if (clk'event and clk ='1') then     
Q <= D; 
end if;  
end process; 
end comporte ;